<style>
ul ul .grey {
            background-color: #DCDCDC; /* фон текста ссылок вложенных списков */
        }

ul li {
    list-style: none; /* убираю дефолтные маркеры */
}

.list span {
    cursor: pointer;
}

.hide::before {
    content: "\2795"; /*вместо маркера поставлю эмоджи*/
}

.show::before {
    content: "\2796"; /*вместо маркера поставлю эмоджи*/
}   
    </style>
<?php

//Устанавливаем кодировку и вывод всех ошибок
header('Content-Type: text/html; charset=UTF-8');
error_reporting(E_ALL);
$host = 'localhost'; // имя хоста
$user = 'root';      // имя пользователя
$pass = '';          // пароль
$name = 'catalog';      // имя базы данных
//Объектно-ориентированный стиль
$mysqli = new mysqli($host, $user, $pass, $name);

//Устанавливаем кодировку utf8
$mysqli->query("SET NAMES 'utf8'");

/* Это "официальный" объектно-ориентированный способ сделать это */
if ($mysqli->connect_error) {
    die('Ошибка подключения (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

//Получаем наш каталог из БД в виде массива
function getCat($mysqli){
    $sql = 'SELECT * FROM `categories`';
    $res = $mysqli->query($sql);

    //Создаем масив где его ключом является ID меню
    $cat = array();
    while($row = $res->fetch_assoc()){
        $cat[$row['id']] = $row;
    }
    return $cat;
}

//Функция построения дерева из массива от Tommy Lacroix
function getTree($dataset) {
    $tree = array();

    foreach ($dataset as $id => &$node) {    
        //Если нет вложений
        if (!$node['parent']){
            $tree[$id] = &$node;
        }else{ 
            //Если есть потомки то перебераем массив
            $dataset[$node['parent']]['childs'][$id] = &$node;
        }
    }
    return $tree;
}

//Получаем подготовленный массив с данными
$cat  = getCat($mysqli); 

//Создаем древовидное меню
$tree = getTree($cat);

//Шаблон для вывода меню в виде дерева
function tplMenu($category){
    $menu = '<li>
        <a href="#" title="'. $category['title'] .'" class="grey" style="color:black; text-decoration:none">'. 
        $category['title'].'</a>';
        
        if(isset($category['childs'])){
            $menu .= '<ul>'. showCat($category['childs']) .'</ul>';
        }
    $menu .= '</li>';
    
    return $menu;
}

//Рекурсивно считываем наш шаблон
function showCat($data){
    $string = '';
    foreach($data as $item){
        $string .= tplMenu($item);
    }
    return $string;
}

//Получаем HTML разметку
$cat_menu = showCat($tree);

//Выводим на экран
echo '<ul class="list" id="list">'. $cat_menu .'</ul>';

?>
<!-- Скрипт для сворачивания/разворачивания категорий -->
<script type='text/javascript'>
for (let li of list.querySelectorAll("li")) {
  let span = document.createElement("span");
  span.classList.add("show");
  li.prepend(span);
  span.append(span.nextSibling);
}

list.onclick = function (event) {
  if (event.target.tagName != "SPAN") return;

  let childrenList = event.target.parentNode.querySelector("ul");
  if (!childrenList) return;
  childrenList.hidden = !childrenList.hidden;

  if (childrenList.hidden) {
    event.target.classList.add("hide");
    event.target.classList.remove("show");
  } else {
    event.target.classList.add("show");
    event.target.classList.remove("hide");
  }
};
</script>
